package pl.inspeerity.trees;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class LeafBranch extends AbstractBranch {

    private final List<Leaf> leaves = new ArrayList<>();

    public LeafBranch(float diameter, float length) {
        super(diameter, length);
        addLeaves();
    }

    void grow() {
        super.grow();
        addLeaves();
    }

    void addLeaves() {
        for (int i = 0; i < randomCountOfLeavesOrNeedles(); i++) {
            getLeaves().add(new Leaf());
        }
    }

    public List<Leaf> getLeaves() {
        return leaves;
    }

    @Override
    public String toString() {
        return "LeafBranch{" +
                "diameter=" + getDiameter() +
                ", length=" + getLength() +
                ", leaves=" + leaves +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LeafBranch)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        LeafBranch that = (LeafBranch) o;
        return Objects.equals(leaves, that.leaves);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), leaves);
    }
}
