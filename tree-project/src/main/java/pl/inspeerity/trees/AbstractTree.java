package pl.inspeerity.trees;

import java.util.List;
import java.util.Objects;

abstract class AbstractTree<T extends AbstractBranch> {

    private final Trunk trunk;
    private final List<T> branches;

    AbstractTree(Trunk trunk, List<T> branches) {
        this.trunk = trunk;
        this.branches = branches;
    }

    public abstract void grow(SeasonType season);

    public Trunk getTrunk() {
        return this.trunk;
    }

    public List<T> getBranches() {
        return this.branches;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AbstractTree)) {
            return false;
        }
        AbstractTree<?> that = (AbstractTree<?>) o;
        return Objects.equals(trunk, that.trunk) && Objects.equals(branches, that.branches);
    }

    @Override
    public int hashCode() {
        return Objects.hash(trunk, branches);
    }
}
