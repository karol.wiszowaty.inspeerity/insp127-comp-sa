package pl.inspeerity.trees.main;

import java.util.ArrayList;
import java.util.List;

import pl.inspeerity.trees.ConiferousTree;
import pl.inspeerity.trees.DeciduousTree;
import pl.inspeerity.trees.LeafBranch;
import pl.inspeerity.trees.NeedleBranch;
import pl.inspeerity.trees.SeasonType;
import pl.inspeerity.trees.Trunk;

public class TreeApplication {

    public static void main(String[] args) {

        List<NeedleBranch> needleBranches = new ArrayList<>();
        needleBranches.add(new NeedleBranch(0.11F, 2.1F));
        needleBranches.add(new NeedleBranch(0.12F, 2.2F));
        needleBranches.add(new NeedleBranch(0.13F, 2.3F));
        ConiferousTree coniferousTree = new ConiferousTree(new Trunk(2, 15), needleBranches);

        System.out.println("coniferousTree: \n" + coniferousTree);
        coniferousTree.grow(SeasonType.SPRING);
        System.out.println("coniferousTree after SPRING: \n" + coniferousTree);
        coniferousTree.grow(SeasonType.AUTUMN);
        System.out.println("coniferousTree AUTUMN : \n" + coniferousTree);
        System.out.println("\n\n");

        List<LeafBranch> leafBranches = new ArrayList<>();
        leafBranches.add(new LeafBranch(0.1F, 2));
        leafBranches.add(new LeafBranch(0.15F, 2.3F));
        leafBranches.add(new LeafBranch(0.12F, 1.7F));
        leafBranches.add(new LeafBranch(0.15F, 1.3F));
        DeciduousTree deciduousTree = new DeciduousTree(new Trunk(2.5F, 25), leafBranches);

        System.out.println("deciduousTree: \n" + deciduousTree);
        deciduousTree.grow(SeasonType.SPRING);
        System.out.println("deciduousTree after SPRING: \n" + deciduousTree);
        deciduousTree.grow(SeasonType.AUTUMN);
        System.out.println("deciduousTree after AUTUMN: \n" + deciduousTree);
        deciduousTree.grow(SeasonType.WINTER);
        System.out.println("deciduousTree after WINTER: \n" + deciduousTree);
        deciduousTree.grow(SeasonType.SPRING);
        System.out.println("deciduousTree after SPRING: \n" + deciduousTree);

        deciduousTree.equals(coniferousTree);
    }
}
