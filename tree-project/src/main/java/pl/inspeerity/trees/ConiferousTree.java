package pl.inspeerity.trees;

import java.util.List;

public class ConiferousTree extends AbstractTree<NeedleBranch> {

    public ConiferousTree(Trunk trunk, List<NeedleBranch> branches) {
        super(trunk, branches);
    }

    @Override
    public void grow(SeasonType season) {
        if (SeasonType.SPRING.equals(season)) {
            getTrunk().grow();
            getBranches().forEach(NeedleBranch::grow);
            getBranches().add(new NeedleBranch(0.01F, 0.1F));
        }
    }

    @Override
    public String toString() {
        return "ConiferousTree{" +
                "trunk=" + getTrunk() +
                ", branches=" + getBranches() +
                '}';
    }

}
