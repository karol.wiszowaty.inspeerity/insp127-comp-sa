package pl.inspeerity.trees;

import java.util.Objects;

public class Trunk {

    private static final float DIAMETER_GROWTH = 1.01F;
    private static final float HEIGHT_GROWTH = 1.02F;

    private float diameter;
    private float height;

    public Trunk(float diameter, float height) {
        this.diameter = diameter;
        this.height = height;
    }

    void grow() {
        setDiameter(getDiameter() * DIAMETER_GROWTH);
        setHeight(getHeight() * HEIGHT_GROWTH);
    }

    public float getDiameter() {
        return diameter;
    }

    private void setDiameter(float diameter) {
        this.diameter = diameter;
    }

    public float getHeight() {
        return height;
    }

    private void setHeight(float height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "Trunk{" +
                "diameter=" + diameter +
                ", height=" + height +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Trunk)) {
            return false;
        }
        Trunk trunk = (Trunk) o;
        return Float.compare(trunk.diameter, diameter) == 0 && Float.compare(trunk.height, height) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(diameter, height);
    }
}
