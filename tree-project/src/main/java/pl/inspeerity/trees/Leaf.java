package pl.inspeerity.trees;

import java.util.Objects;

public class Leaf {

    private LeafColorType color = LeafColorType.GREEN;

    public LeafColorType getColor() {
        return color;
    }

    void changeColor(SeasonType season) {
        switch (season) {
            case AUTUMN:
                setColor(LeafColorType.YELLOW);
                break;
            default:
                setColor(LeafColorType.GREEN);
        }
    }

    private void setColor(LeafColorType color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Leaf{" +
                "color=" + color +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Leaf leaf = (Leaf) o;
        return color == leaf.color;
    }

    @Override
    public int hashCode() {
        return Objects.hash(color);
    }
}
