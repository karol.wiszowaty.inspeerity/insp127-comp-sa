package pl.inspeerity.trees;

import java.util.Objects;

public class Needle {

    static final float MIN_LENGTH = 0.01F;
    static final float MAX_LENGTH = 0.05F;

    private final float length;

    Needle(float length) {
        this.length = length;
    }

    public float getLength() {
        return length;
    }

    @Override
    public String toString() {
        return "Needle{" +
                "length=" + length +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Needle)) {
            return false;
        }
        Needle needle = (Needle) o;
        return Float.compare(needle.length, length) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(length);
    }
}
