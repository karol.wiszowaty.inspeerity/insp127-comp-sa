package pl.inspeerity.trees;

import java.util.List;

public class DeciduousTree extends AbstractTree<LeafBranch> {

    public DeciduousTree(Trunk trunk, List<LeafBranch> branches) {
        super(trunk, branches);
    }

    @Override
    public void grow(SeasonType season) {
        switch (season) {
            case SPRING:
                grow();
                break;
            case AUTUMN:
                changeColor();
                break;
            case WINTER:
                shedLeaves();
                break;
        }
    }

    void grow() {
        getTrunk().grow();
        getBranches().forEach(LeafBranch::grow);
        getBranches().forEach(leafBranch -> leafBranch.getLeaves().forEach(leaf -> leaf.changeColor(SeasonType.SPRING)));
        getBranches().add(new LeafBranch(0.01F, 0.1F));
    }

    void changeColor() {
        getBranches().forEach(leafBranch -> leafBranch.getLeaves().forEach(leaf -> leaf.changeColor(SeasonType.AUTUMN)));
    }

    void shedLeaves() {
        getBranches().forEach(leafBranch -> leafBranch.getLeaves().clear());
    }

    @Override
    public String toString() {
        return "DeciduousTree{" +
                "trunk=" + getTrunk() +
                ", branches=" + getBranches() +
                '}';
    }
}
