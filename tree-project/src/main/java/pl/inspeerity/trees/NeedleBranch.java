package pl.inspeerity.trees;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;

public class NeedleBranch extends AbstractBranch {

    private final List<Needle> needles = new ArrayList<>();

    public NeedleBranch(float diameter, float length) {
        super(diameter, length);
        addNeedles();
    }

    void grow() {
        super.grow();
        addNeedles();
    }

    void addNeedles() {
        for (int i = 0; i < randomCountOfLeavesOrNeedles(); i++) {
            getNeedles().add(new Needle(ThreadLocalRandom.current().nextFloat() * (Needle.MAX_LENGTH - Needle.MIN_LENGTH)));
        }
    }

    public List<Needle> getNeedles() {
        return needles;
    }

    @Override
    public String toString() {
        return "NeedleBranch{" +
                "diameter=" + getDiameter() +
                ", length=" + getLength() +
                ", needles=" + needles +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NeedleBranch)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        NeedleBranch that = (NeedleBranch) o;
        return Objects.equals(needles, that.needles);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), needles);
    }
}
