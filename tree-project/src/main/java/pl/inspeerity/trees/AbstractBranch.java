package pl.inspeerity.trees;

import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;

abstract class AbstractBranch {

    private static final float DIAMETER_GROWTH = 1.02F;
    private static final float LENGTH_GROWTH = 1.03F;
    private static final int MIN_NEW_LEAVES_OR_NEEDLES = 1;
    private static final int MAX_NEW_LEAVES_OR_NEEDLES = 5;

    private float diameter;
    private float length;

    AbstractBranch(float diameter, float length) {
        this.diameter = diameter;
        this.length = length;
    }

    void grow() {
        setDiameter(getDiameter() * DIAMETER_GROWTH);
        setLength(getLength() * LENGTH_GROWTH);
    }

    int randomCountOfLeavesOrNeedles() {
        return ThreadLocalRandom.current().nextInt(MIN_NEW_LEAVES_OR_NEEDLES, MAX_NEW_LEAVES_OR_NEEDLES + 1);
    }

    public float getDiameter() {
        return diameter;
    }

    public float getLength() {
        return length;
    }

    private void setDiameter(float diameter) {
        this.diameter = diameter;
    }

    private void setLength(float length) {
        this.length = length;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AbstractBranch that = (AbstractBranch) o;
        return Float.compare(that.diameter, diameter) == 0 && Float.compare(that.length, length) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(diameter, length);
    }
}
