package pl.inspeerity.georest.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeoRestAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(GeoRestAppApplication.class, args);
    }

}
