package pl.inspeerity.georest.app.repository.rest;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import pl.inspeerity.georest.app.model.GeoLocation;

@RepositoryRestResource(collectionResourceRel = "location", path = "location")
public interface GeoLocationRepositoryController extends CrudRepository<GeoLocation, Long> {

}
