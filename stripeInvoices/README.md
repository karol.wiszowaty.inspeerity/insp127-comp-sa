Aplikacja do tworzenia i pobierania faktur w formacie PDF. Wykorzystuje domyślne konto testowe dostarczane przez Stripe.

Technologie: Spring Boot 2.5.5, stripe-java 20.79.0. Frontend został oparty na Thymeleaf.

Moduły:
* configuration - klasy z konfiguracją dla całej aplikacji
* model - klasy modelu
* service - klasa udostępniająca metody związane z fakturami; komunikuje się z API Stripe i przetwarza odpowiedzi na odpowiedni format
* controller - klasy odpowiadające za obsługę żądań wysyłanych przez frontend oraz obsługę błędów
* StripeInvoicesApplication - główna klasa aplikacji Spring Boot

Dodatkowa konfiguracja znajduję się w pliku /src/main/resources/application.properties.
Pliki z logami są tworzone w katalogu /log.

Testy jednostkowe znajdują się w klasie InvoiceServiceTest, a integracyjne w InvoiceItemControllerTests. Testy integracyjne są niepełne,
bo korzystamy z domyślnego konta testowego co mogło powodować ich nieprawidłowe wyniki.

Frontend został przygotowany w uproszczonej formie, tylko w celu podstawowego podstawowej prezentacji danych i dostępnych operacji.
Główna strona znajduje się pod adresem http://localhost:8080/.

Działanie:
* ponieważ korzystamy z domyslnego konta testowego mogą się pojawiac faktury, których użytkownik sam nie stworzył
* aby utworzyć fakturę (Invoice) muszą być wcześniej stworzone obiekty InvoiceItem
* sfinalizowana fakturę można pobrać w formacie PDF; odnośnik kieruje na stronę Stripe

