package pl.inspeerity.stripe.invoices;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import pl.inspeerity.stripe.invoices.configuration.StripeProperties;
import pl.inspeerity.stripe.invoices.model.InvoiceItemCreate;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class InvoiceItemControllerTests {

    private static final String GO_TO_START_PAGE_TEXT = "Go to start page";

    @LocalServerPort
    private int port;

    private String url;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private StripeProperties stripeProperties;

    @BeforeEach
    void initUseCase() {
        url = "http://localhost:" + port;
    }

    @Test
    public void whenCallNotExistingPage_thenReturn404() {
        ResponseEntity<String> response = sentGetRequest("/not_existing_page");

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody()).contains(String.valueOf(HttpStatus.NOT_FOUND.value()));
        assertThat(response.getBody()).contains(HttpStatus.NOT_FOUND.getReasonPhrase());
        assertThat(response.getBody()).contains(GO_TO_START_PAGE_TEXT);
    }

    @Test
    public void givenBadMethodType_whenCallInvoiceItem_thenReturn405() {
        ResponseEntity<String> response = sentGetRequest("/invoiceitem");

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.METHOD_NOT_ALLOWED);
        assertThat(response.getBody()).contains(String.valueOf(HttpStatus.METHOD_NOT_ALLOWED.value()));
        assertThat(response.getBody()).contains(HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase());
        assertThat(response.getBody()).contains(GO_TO_START_PAGE_TEXT);
    }

    @Test
    public void givenInvoiceItemCreateWithBadData_whenCallInvoiceItem_thenReturnValidationErrors() {
        InvoiceItemCreate invoiceItemCreate = new InvoiceItemCreate(0L, "description_is_to_loooooooong");
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("amount", invoiceItemCreate.getAmount().toString());
        params.add("description", invoiceItemCreate.getDescription());
        ResponseEntity<String> response = sentPostRequest("/invoiceitem", params);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).contains("InvoiceItems");
        assertThat(response.getBody()).contains(stripeProperties.getCustomer());
        assertThat(response.getBody()).contains("Amount must be greater than 0");
        assertThat(response.getBody()).contains("Description must be no more than 20 characters long");
    }

    @Test
    public void givenInvoiceItemCreateWithGoodData_whenCallInvoiceItem_thenReturnInvoiceItemList() {
        InvoiceItemCreate invoiceItemCreate = new InvoiceItemCreate(2345L, "description11");
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("amount", invoiceItemCreate.getAmount().toString());
        params.add("description", invoiceItemCreate.getDescription());
        ResponseEntity<String> response = sentPostRequest("/invoiceitem", params);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).contains("InvoiceItems");
        assertThat(response.getBody()).contains(stripeProperties.getCustomer());
        assertThat(response.getBody()).contains("23.45");
        assertThat(response.getBody()).contains("description11");
    }


    private ResponseEntity<String> sentGetRequest(String path) {
        return restTemplate.getForEntity(url + path, String.class);
    }

    private ResponseEntity<String> sentPostRequest(String path, MultiValueMap<String, String> params) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(params, headers);
        return restTemplate.postForEntity(url + path, request, String.class);
    }

}
