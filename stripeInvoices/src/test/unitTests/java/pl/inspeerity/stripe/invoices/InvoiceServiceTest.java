package pl.inspeerity.stripe.invoices;

import java.util.LinkedList;
import java.util.List;

import com.stripe.exception.StripeException;
import com.stripe.model.Invoice;
import com.stripe.model.InvoiceCollection;
import com.stripe.model.InvoiceItem;
import com.stripe.model.InvoiceItemCollection;
import com.stripe.param.InvoiceCreateParams;
import com.stripe.param.InvoiceItemCreateParams;
import com.stripe.param.InvoiceItemListParams;
import com.stripe.param.InvoiceListParams;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.inspeerity.stripe.invoices.configuration.StripeProperties;
import pl.inspeerity.stripe.invoices.model.InvoiceItemCreate;
import pl.inspeerity.stripe.invoices.model.dto.InvoiceDto;
import pl.inspeerity.stripe.invoices.model.dto.InvoiceItemDto;
import pl.inspeerity.stripe.invoices.service.InvoiceService;
import pl.inspeerity.stripe.invoices.service.impl.InvoiceServiceImpl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertNull;

@ExtendWith(MockitoExtension.class)
public class InvoiceServiceTest {

    @Mock
    private StripeProperties stripeProperties;

    private InvoiceService invoiceService;

    @BeforeEach
    void initUseCase() {
        invoiceService = new InvoiceServiceImpl(stripeProperties);
    }

    @Test
    void givenInvoice_whenCreateInvoice_thenReturnInvoiceDto() throws StripeException {
        Invoice invoice = new Invoice();
        invoice.setId("invoiceId");
        invoice.setCustomer("customer");
        invoice.setAmountRemaining(25L);

        try (MockedStatic<Invoice> invoiceClass = Mockito.mockStatic(Invoice.class)) {
            invoiceClass.when(() -> Invoice.create(any(InvoiceCreateParams.class))).thenReturn(invoice);

            InvoiceDto invoiceDto = invoiceService.createInvoice();
            assertEquals("Check invoiceId", "invoiceId", invoiceDto.getId());
            assertEquals("Check customer", "customer", invoiceDto.getCustomer());
            assertEquals("Check amountRemaining", 25L, invoiceDto.getAmountRemaining());
            assertNull("Check invoicePdf", invoiceDto.getInvoicePdf());
        }
    }

    @Test
    void givenInvoice_whenFindInvoice_thenReturnInvoiceDto() throws StripeException {
        Invoice invoice = new Invoice();
        invoice.setId("invoiceId2");
        invoice.setCustomer("customer2");

        try (MockedStatic<Invoice> invoiceClass = Mockito.mockStatic(Invoice.class)) {
            invoiceClass.when(() -> Invoice.retrieve(anyString())).thenReturn(invoice);

            InvoiceDto invoiceDto = invoiceService.findInvoiceById("invoiceId2");
            assertEquals("Check invoiceId", "invoiceId2", invoiceDto.getId());
            assertEquals("Check customer", "customer2", invoiceDto.getCustomer());
            assertNull("Check amountRemaining", invoiceDto.getAmountRemaining());
            assertNull("Check invoicePdf", invoiceDto.getInvoicePdf());
        }
    }

    @Test
    void givenInvoices_whenFindInvoices_thenReturnInvoicesDtos() throws StripeException {
        InvoiceCollection invoiceCollection = new InvoiceCollection();
        List<Invoice> invoices = new LinkedList<>();
        for (int i = 0; i < 5; i++) {
            Invoice invoice = new Invoice();
            invoice.setId("invoiceId" + i);
            invoice.setCustomer("customer" + i);
            invoice.setAmountRemaining(25L + i);
            invoices.add(invoice);
        }
        invoiceCollection.setData(invoices);

        try (MockedStatic<Invoice> invoiceClass = Mockito.mockStatic(Invoice.class)) {
            invoiceClass.when(() -> Invoice.list(any(InvoiceListParams.class))).thenReturn(invoiceCollection);

            List<InvoiceDto> invoiceDtos = invoiceService.findInvoices();
            for (int i = 0; i < 5; i++) {
                InvoiceDto invoiceDto = invoiceDtos.get(i);
                assertEquals("Check invoiceId", "invoiceId" + i, invoiceDto.getId());
                assertEquals("Check customer", "customer" + i, invoiceDto.getCustomer());
                assertEquals("Check amountRemaining", 25L + i, invoiceDto.getAmountRemaining());
                assertNull("Check invoicePdf", invoiceDto.getInvoicePdf());
            }
        }
    }

    @Test
    void givenInvoice_whenFinalizeInvoice_thenReturnInvoiceDto() throws StripeException {
        Invoice invoice = mock(Invoice.class);
        Mockito.when(invoice.getId()).thenReturn("invoiceId");
        Mockito.when(invoice.getCustomer()).thenReturn("customer");
        Mockito.when(invoice.getAmountRemaining()).thenReturn(25L);
        Mockito.when(invoice.getInvoicePdf()).thenReturn("https://test.url");

        try (MockedStatic<Invoice> invoiceClass = Mockito.mockStatic(Invoice.class)) {
            invoiceClass.when(() -> Invoice.retrieve(anyString())).thenReturn(invoice);
            Mockito.when(invoice.finalizeInvoice()).thenReturn(invoice);

            InvoiceDto invoiceDto = invoiceService.finalizeInvoice(invoice.getId());
            assertEquals("Check invoiceId", "invoiceId", invoiceDto.getId());
            assertEquals("Check customer", "customer", invoiceDto.getCustomer());
            assertEquals("Check amountRemaining", 25L, invoiceDto.getAmountRemaining());
            assertEquals("Check invoicePdf", "https://test.url", invoiceDto.getInvoicePdf());
        }
    }

    @Test
    void givenInvoiceItem_whenCreateInvoiceItem_thenReturnInvoiceItemDto() throws StripeException {
        InvoiceItemCreate invoiceItemCreate = new InvoiceItemCreate(25L, "description");

        InvoiceItem invoiceItem = new InvoiceItem();
        invoiceItem.setId("invoiceItemId");
        invoiceItem.setCustomer("customer");
        invoiceItem.setAmount(invoiceItemCreate.getAmount());
        invoiceItem.setDescription(invoiceItemCreate.getDescription());

        try (MockedStatic<InvoiceItem> invoiceItemClass = Mockito.mockStatic(InvoiceItem.class)) {
            invoiceItemClass.when(() -> InvoiceItem.create(any(InvoiceItemCreateParams.class))).thenReturn(invoiceItem);

            InvoiceItemDto invoiceItemDto = invoiceService.createInvoiceItem(invoiceItemCreate);
            assertEquals("Check invoiceId", "invoiceItemId", invoiceItemDto.getId());
            assertEquals("Check customer", "customer", invoiceItemDto.getCustomer());
            assertEquals("Check amountRemaining", 25L, invoiceItemDto.getAmount());
            assertEquals("Check description", "description", invoiceItemDto.getDescription());
        }
    }

    @Test
    void givenInvoiceItems_whenFindInvoiceItems_thenReturnInvoiceItemDtos() throws StripeException {
        InvoiceItemCollection invoiceItemCollection = new InvoiceItemCollection();
        List<InvoiceItem> invoiceItems = new LinkedList<>();
        for (int i = 0; i < 5; i++) {
            InvoiceItem invoiceItem = new InvoiceItem();
            invoiceItem.setId("invoiceId" + i);
            invoiceItem.setCustomer("customer" + i);
            invoiceItem.setAmount(25L + i);
            invoiceItems.add(invoiceItem);
        }
        invoiceItemCollection.setData(invoiceItems);

        try (MockedStatic<InvoiceItem> invoiceItemClass = Mockito.mockStatic(InvoiceItem.class)) {
            invoiceItemClass.when(() -> InvoiceItem.list(any(InvoiceItemListParams.class))).thenReturn(invoiceItemCollection);

            List<InvoiceItemDto> invoiceItemDtos = invoiceService.findInvoiceItems();
            for (int i = 0; i < 5; i++) {
                InvoiceItemDto invoiceItemDto = invoiceItemDtos.get(i);
                assertEquals("Check invoiceId", "invoiceId" + i, invoiceItemDto.getId());
                assertEquals("Check customer", "customer" + i, invoiceItemDto.getCustomer());
                assertEquals("Check amountRemaining", 25L + i, invoiceItemDto.getAmount());
            }
        }
    }

}
