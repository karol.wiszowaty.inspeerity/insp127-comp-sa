package pl.inspeerity.stripe.invoices.service;

import java.util.List;

import com.stripe.exception.StripeException;
import pl.inspeerity.stripe.invoices.model.dto.InvoiceDto;
import pl.inspeerity.stripe.invoices.model.InvoiceItemCreate;
import pl.inspeerity.stripe.invoices.model.dto.InvoiceItemDto;

public interface InvoiceService {

    InvoiceDto createInvoice() throws StripeException;

    InvoiceDto findInvoiceById(String id) throws StripeException;

    List<InvoiceDto> findInvoices() throws StripeException;

    InvoiceDto finalizeInvoice(String id) throws StripeException;

    InvoiceItemDto createInvoiceItem(InvoiceItemCreate invoiceItemCreate) throws StripeException;

    List<InvoiceItemDto> findInvoiceItems() throws StripeException;
}
