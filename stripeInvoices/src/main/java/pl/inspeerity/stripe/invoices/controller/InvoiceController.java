package pl.inspeerity.stripe.invoices.controller;

import java.util.List;

import com.stripe.exception.StripeException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.thymeleaf.util.StringUtils;
import pl.inspeerity.stripe.invoices.configuration.StripeProperties;
import pl.inspeerity.stripe.invoices.configuration.utils.WebUtils;
import pl.inspeerity.stripe.invoices.model.dto.InvoiceDto;
import pl.inspeerity.stripe.invoices.service.InvoiceService;

@Controller
@RequiredArgsConstructor
public class InvoiceController {

    private final InvoiceService invoiceService;
    private final StripeProperties stripeProperties;

    @ModelAttribute
    private void addModelAttributes(Model model) {
        model.addAttribute(WebUtils.MODEL_CUSTOMER, stripeProperties.getCustomer());
    }

    @GetMapping("/invoices")
    public String showInvoices(Model model) throws StripeException {
        model.addAttribute(WebUtils.MODEL_INVOICES, invoiceService.findInvoices());
        return "invoice";
    }

    @PostMapping("/invoice")
    public String createInvoice(Model model) throws StripeException {
        InvoiceDto invoiceDto = invoiceService.createInvoice();
        model.addAttribute(WebUtils.MODEL_INVOICES, List.of(invoiceDto));
        return "invoice";
    }

    @GetMapping("/invoice")
    public String findInvoice(Model model, @RequestParam(name = "id") String invoiceId) throws StripeException {
        if (!StringUtils.isEmptyOrWhitespace(invoiceId)) {
            model.addAttribute(WebUtils.MODEL_INVOICES, List.of(invoiceService.findInvoiceById(invoiceId)));
        }
        return "invoice";
    }

    @GetMapping("/invoice/finalize/{id}")
    public String finalizeInvoice(@PathVariable(name = "id") String invoiceId) throws StripeException {
        invoiceService.finalizeInvoice(invoiceId);
        return "redirect:/invoices";
    }
}
