package pl.inspeerity.stripe.invoices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StripeInvoicesApplication {

    public static void main(String[] args) {
        SpringApplication.run(StripeInvoicesApplication.class, args);
    }

}
