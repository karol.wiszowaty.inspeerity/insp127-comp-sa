package pl.inspeerity.stripe.invoices.configuration;

import javax.validation.constraints.NotBlank;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Data
@Configuration
@ConfigurationProperties(prefix = "stripe")
@Validated
public class StripeProperties {

    private @NotBlank String apiKey;
    private @NotBlank String customer;

}
