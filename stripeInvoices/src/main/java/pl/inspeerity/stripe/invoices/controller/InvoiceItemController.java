package pl.inspeerity.stripe.invoices.controller;

import javax.validation.Valid;

import com.stripe.exception.StripeException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pl.inspeerity.stripe.invoices.configuration.StripeProperties;
import pl.inspeerity.stripe.invoices.configuration.utils.WebUtils;
import pl.inspeerity.stripe.invoices.model.InvoiceItemCreate;
import pl.inspeerity.stripe.invoices.service.InvoiceService;

@Controller
@RequiredArgsConstructor
public class InvoiceItemController {

    private final InvoiceService invoiceService;
    private final StripeProperties stripeProperties;

    @ModelAttribute
    private void addModelAttributes(Model model) {
        model.addAttribute(WebUtils.MODEL_CUSTOMER, stripeProperties.getCustomer());
    }

    @GetMapping("/")
    public String showInvoiceItems(Model model) throws StripeException {
        model.addAttribute("invoiceItemCreate", new InvoiceItemCreate(0L, ""));
        model.addAttribute(WebUtils.MODEL_INVOICE_ITEMS, invoiceService.findInvoiceItems());
        return "invoiceItem";
    }

    @PostMapping("/invoiceitem")
    public String createInvoiceItem(Model model, @Valid InvoiceItemCreate invoiceItemCreate, BindingResult result) throws StripeException {
        if (!result.hasErrors()) {
            invoiceService.createInvoiceItem(invoiceItemCreate);
        }
        model.addAttribute(WebUtils.MODEL_INVOICE_ITEMS, invoiceService.findInvoiceItems());
        return "invoiceItem";
    }
}
