package pl.inspeerity.stripe.invoices.service.impl;

import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Invoice;
import com.stripe.model.InvoiceItem;
import com.stripe.param.InvoiceCreateParams;
import com.stripe.param.InvoiceItemCreateParams;
import com.stripe.param.InvoiceItemListParams;
import com.stripe.param.InvoiceListParams;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.inspeerity.stripe.invoices.configuration.StripeProperties;
import pl.inspeerity.stripe.invoices.model.dto.InvoiceDto;
import pl.inspeerity.stripe.invoices.model.InvoiceItemCreate;
import pl.inspeerity.stripe.invoices.model.dto.InvoiceItemDto;
import pl.inspeerity.stripe.invoices.service.InvoiceService;

@Service
@Slf4j
@RequiredArgsConstructor
public class InvoiceServiceImpl implements InvoiceService {

    private static final long SEARCH_LIMIT = 20;

    private final StripeProperties stripeProperties;

    @PostConstruct
    public void init() {
        Stripe.apiKey = stripeProperties.getApiKey();
    }

    @Override
    public InvoiceDto createInvoice() throws StripeException {
        log.info("Creating invoice...");
        InvoiceCreateParams invoiceCreateParams = InvoiceCreateParams.builder()
                .setCustomer(stripeProperties.getCustomer())
                .build();
        Invoice invoice = Invoice.create(invoiceCreateParams);
        log.info("Invoice created: " + invoice);
        return new InvoiceDto(invoice);
    }

    @Override
    public InvoiceDto findInvoiceById(String id) throws StripeException {
        log.info("Getting invoice by id: " + id);
        return new InvoiceDto(Invoice.retrieve(id));
    }

    @Override
    public List<InvoiceDto> findInvoices() throws StripeException {
        log.info("Getting invoices for customer: " + stripeProperties.getCustomer());
        List<InvoiceDto> invoices = Invoice.list(
                        InvoiceListParams.builder()
                                .setCustomer(stripeProperties.getCustomer())
                                .setLimit(SEARCH_LIMIT)
                                .build()
                ).getData().stream()
                .map(InvoiceDto::new)
                .collect(Collectors.toList());
        log.info("Found {} invoices for customer {} ", invoices.size(), stripeProperties.getCustomer());
        return invoices;
    }

    @Override
    public InvoiceDto finalizeInvoice(String id) throws StripeException {
        log.info("Finalizing invoice id: " + id);
        return new InvoiceDto(Invoice.retrieve(id).finalizeInvoice());
    }

    @Override
    public InvoiceItemDto createInvoiceItem(InvoiceItemCreate invoiceItemCreate) throws StripeException {
        log.info("Creating invoiceItem: " + invoiceItemCreate);
        InvoiceItemCreateParams invoiceItemCreateParams = InvoiceItemCreateParams.builder()
                .setCustomer(stripeProperties.getCustomer())
                .setAmount(invoiceItemCreate.getAmount())
                .setCurrency("USD")
                .setDescription(invoiceItemCreate.getDescription())
                .build();
        InvoiceItem invoiceItem = InvoiceItem.create(invoiceItemCreateParams);
        log.info("InvoiceItem created: " + invoiceItem);
        return new InvoiceItemDto(invoiceItem);

    }

    @Override
    public List<InvoiceItemDto> findInvoiceItems() throws StripeException {
        log.info("Getting invoiceItems for customer: " + stripeProperties.getCustomer());
        List<InvoiceItemDto> invoiceItemDtoList = InvoiceItem.list(
                        InvoiceItemListParams.builder()
                                .setCustomer(stripeProperties.getCustomer())
                                .setLimit(SEARCH_LIMIT)
                                .build()
                ).getData().stream()
                .map(InvoiceItemDto::new)
                .collect(Collectors.toList());
        log.info("Found {} invoiceItems for customer {} ", invoiceItemDtoList.size(), stripeProperties.getCustomer());
        return invoiceItemDtoList;
    }
}
