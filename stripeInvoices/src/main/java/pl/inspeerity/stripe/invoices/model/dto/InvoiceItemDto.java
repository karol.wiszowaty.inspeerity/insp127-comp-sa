package pl.inspeerity.stripe.invoices.model.dto;

import com.stripe.model.InvoiceItem;
import lombok.Value;

@Value
public class InvoiceItemDto {

    String id;
    String customer;
    Long amount;
    String description;
    String invoiceId;

    public InvoiceItemDto(InvoiceItem invoiceItem) {
        this.id = invoiceItem.getId();
        this.customer = invoiceItem.getCustomer();
        this.amount = invoiceItem.getAmount();
        this.description = invoiceItem.getDescription();
        this.invoiceId = invoiceItem.getInvoice();
    }
}
