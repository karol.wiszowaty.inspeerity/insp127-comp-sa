package pl.inspeerity.stripe.invoices.controller.error;

import com.stripe.exception.StripeException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import pl.inspeerity.stripe.invoices.configuration.utils.WebUtils;

@ControllerAdvice
@Slf4j
public class GlobalControllerExceptionHandler {

    @ExceptionHandler(StripeException.class)
    public String handleError(Model model, StripeException ex) {
        String exceptionMessage = ex.getMessage();
        log.error("Stripe communication error {}", exceptionMessage);
        model.addAttribute("errorMessage", exceptionMessage);
        return WebUtils.ERROR_PAGE;
    }
}
