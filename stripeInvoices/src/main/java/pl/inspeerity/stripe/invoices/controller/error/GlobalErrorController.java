package pl.inspeerity.stripe.invoices.controller.error;

import java.util.Objects;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.inspeerity.stripe.invoices.configuration.utils.WebUtils;

@Controller
public class GlobalErrorController implements ErrorController {

    @RequestMapping("/error")
    public String handleError(HttpServletRequest request, Model model) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        HttpStatus httpStatus = Objects.requireNonNullElse(HttpStatus.resolve((Integer) status), HttpStatus.NOT_FOUND);
        model.addAttribute("errorCode", httpStatus.value());
        model.addAttribute("errorMessage", httpStatus.getReasonPhrase());
        return WebUtils.ERROR_PAGE;
    }
}
