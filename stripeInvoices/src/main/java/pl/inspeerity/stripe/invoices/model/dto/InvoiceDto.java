package pl.inspeerity.stripe.invoices.model.dto;

import com.stripe.model.Invoice;
import lombok.Value;

@Value
public class InvoiceDto {

    String id;
    String customer;
    Long amountRemaining;
    String invoicePdf;

    public InvoiceDto(Invoice invoice) {
        this.id = invoice.getId();
        this.customer = invoice.getCustomer();
        this.amountRemaining = invoice.getAmountRemaining();
        this.invoicePdf = invoice.getInvoicePdf();
    }
}
