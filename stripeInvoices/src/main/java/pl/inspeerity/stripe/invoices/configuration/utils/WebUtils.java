package pl.inspeerity.stripe.invoices.configuration.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class WebUtils {

    public final String ERROR_PAGE = "errors/error";

    public final String MODEL_CUSTOMER = "customer";
    public final String MODEL_INVOICES = "invoices";
    public final String MODEL_INVOICE_ITEMS = "invoiceItems";
}
