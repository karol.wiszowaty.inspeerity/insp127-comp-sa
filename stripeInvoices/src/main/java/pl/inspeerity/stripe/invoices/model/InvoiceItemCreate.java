package pl.inspeerity.stripe.invoices.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.NumberFormat;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceItemCreate {

    @NotNull(message = "Amount cannot be null.")
    @NumberFormat
    @Min(value = 1, message = "Amount must be greater than 0")
    private Long amount;

    @Size(max = 20, message = "Description must be no more than {max} characters long")
    private String description;
}
